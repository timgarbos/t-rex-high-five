﻿using UnityEngine;
using System.Collections;

public class VelosityDirection : MonoBehaviour {
    public Vector3 Direction;
    public GameObject test;

    void Update()
    {
        Direction = transform.InverseTransformDirection(this.GetComponent<Rigidbody2D>().velocity);
        test.transform.position = Direction;
    }
}
