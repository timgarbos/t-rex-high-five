﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Yoyo : MonoBehaviour {

    [SerializeField]
    float dragSpeed = 5f;
    [SerializeField]
    Rigidbody2D mouse;
    [SerializeField]
    DistanceJoint2D distanceJoint;

    [SerializeField]
    Rigidbody2D yoyo;
    Rigidbody2D rigid;

    [SerializeField]
    List<ChainElement> chain;
    Vector3 lastPos;
    float maxChainLength;

    [SerializeField]
    LineRenderer line;

    bool isStarted;
    Vector3[] linePos;
    [SerializeField]
    Transform speechBubble;
    // Use this for initialization
    void Start()
    {
        linePos = new Vector3[chain.Count];
         rigid = GetComponent<Rigidbody2D>();
        lastPos = rigid.position;
        maxChainLength = (rigid.position - yoyo.position).magnitude * 1.2f;
        line = GetComponent<LineRenderer>();
       
        foreach (var chainPiece in chain)
        {
          
                chainPiece.startDistance = (rigid.position - chainPiece.rigid.position).magnitude * 1.4f;
        
        }
    }

    IEnumerator MoveSpeechBubbleRoutine()
    {
        float endTime = Time.time + 2f;
        while (Time.time < endTime)
        {
            speechBubble.position += Vector3.right * Time.deltaTime * 10f;
            yield return null;
        }
    }

	// Update is called once per frame
	void FixedUpdate() {

        Vector2 worldMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mouse.velocity = (worldMousePos - mouse.position) * 50f;

        
        if(!isStarted)
        {
            foreach (var chainPiece in chain)
            {
                chainPiece.rigid.position = worldMousePos+new Vector2(1.7f,-0.7f);
                chainPiece.rigid.isKinematic = true;
            }
        }
        if (Input.GetMouseButton(0))
        {
            if (!isStarted)
            {
                StartCoroutine(MoveSpeechBubbleRoutine());
                Debug.Log("Yo Yolo!");
                
                isStarted = true;
                foreach (var chainPiece in chain)
                {
                    chainPiece.rigid.position = rigid.position;
                    chainPiece.rigid.isKinematic = false;
                }
            }
            

            distanceJoint.enabled = true;
            rigid.isKinematic = false;
            
            float yoyoDist = (yoyo.position - worldMousePos).magnitude;
            Vector3 diff = worldMousePos - rigid.position;
            float dist = diff.magnitude;
            if (diff.magnitude > 1f)
            {
                diff.Normalize();
            }
            float currentSpeed = dragSpeed;
            if (yoyoDist > maxChainLength)
            {
                Vector2 yoyoDir = (yoyo.position - worldMousePos).normalized;
                yoyo.AddForce(-yoyoDir * 5000f * Time.deltaTime, ForceMode2D.Force);

            }
            foreach (var chainPiece in chain)
            {
                Vector2 dir =(chainPiece.rigid.position - worldMousePos); 
                if(dir.magnitude>chainPiece.startDistance)
                {
                    chainPiece.rigid.position = Vector3.MoveTowards(chainPiece.rigid.position,worldMousePos + dir.normalized * chainPiece.startDistance,Time.deltaTime*60f);

                }
            }
          
            rigid.velocity = diff * Mathf.Min(currentSpeed * Time.deltaTime,40f);
            lastPos = rigid.position;
        }
        else
        {
            distanceJoint.enabled = false;
        }

        //line render
        if (Input.GetMouseButton(0))
        {
            line.SetPosition(0, worldMousePos);
        }
        else
        {
            line.SetPosition(0, chain[0].rigid.position);
        }
            for (int i = 0, c = chain.Count; i < c; i++)
            {
                line.SetPosition(i + 1, chain[i].rigid.position);

            }
       
       
    }
}
