﻿using UnityEngine;
using System.Collections;

public class PlayerBase : MonoBehaviour {

	[SerializeField]float walkSpeed = 1f;
	[SerializeField]float rotateSpeed = 25f;
	[SerializeField]float capsuleWalkerSpeed = 25f;

	[SerializeField]Transform leftFootCapsuleTarget;
	[SerializeField]Transform leftFootRealTarget;
	[SerializeField]Transform rightFootCapsuleTarget;
	[SerializeField]Transform rightFootRealTarget;

	[SerializeField] Transform capsuleWalker;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		float forward = Input.GetAxis ("Vertical");
		float rotate = Input.GetAxis ("Horizontal");
		 
		transform.position += transform.forward * forward*walkSpeed*Time.deltaTime;
		transform.Rotate (Vector3.up,rotate*rotateSpeed*Time.deltaTime, Space.World);
		capsuleWalker.Rotate (Vector3.up, (forward+rotate)*capsuleWalkerSpeed*Time.deltaTime, Space.Self);

		leftFootRealTarget.position = leftFootCapsuleTarget.position;
		rightFootRealTarget.position = rightFootCapsuleTarget.position;
	}
}
