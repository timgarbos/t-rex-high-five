﻿using UnityEngine;
using System.Collections;

public class AudioOnCollision : MonoBehaviour {
    public AudioClip clip;
    float nextPlay;
    void OnCollisionEnter2D(Collision2D col)
    {
        if (Time.time > nextPlay && col.transform.tag.Equals("hard"))
        {
            AudioSource.PlayClipAtPoint(clip, transform.position);
            nextPlay = Time.time + 0.1f;
        }
    }
}
