﻿using UnityEngine;
using System.Collections;

public class EndlessScroller : MonoBehaviour {

    public GameObject Background;
    private GameObject oldBG;
    public Vector3 BackgroundOffset;
    public float speed = 7f;
    public SpawnItem[] SpawnObjects;
    private bool Moving = false;

    public float counter = 0;
    private float NextSpawn = 0;

    void Start()
    {
        Moving = true;
        StartCoroutine(SpawnProb());
    }

    void Update()
    {
        if (Moving)
        {
            counter -= speed * Time.deltaTime;

            if (Background.transform.position.x > 0)
                SpawnNextBackground();

            transform.Translate(Vector3.right * speed * Time.deltaTime);
        }
    }

    IEnumerator SpawnProb()
    {
        while (true)
        {
            SpawnItem newItem = SpawnObjects[Random.Range(0, SpawnObjects.Length)];
            GameObject newGO = Instantiate(newItem.gameObject) as GameObject;

            newGO.transform.position = new Vector3(-30f, Random.Range(newItem.MaxMinYPos.x, newItem.MaxMinYPos.y), 0);
            newGO.transform.parent = Background.transform.parent;

            counter = Random.Range(newItem.MaxMinXPos.x, newItem.MaxMinXPos.y);
            yield return new WaitForSeconds(counter / speed);
        }

    }

    void SpawnNextBackground()
    {
        GameObject go = Instantiate(Background) as GameObject;
        go.transform.parent = Background.transform.parent;
        go.transform.position = Background.transform.position + BackgroundOffset;
        Destroy(oldBG);
        oldBG = Background;
        Background = go;
    }
	
}
